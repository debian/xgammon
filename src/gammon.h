/*  gammon.h

    Copyright (C) 1994	Lambert Klasen & Detlef Steuer
			klasen@asterix.uni-muenster.de
			steuer@amadeus.statistik.uni-dortmund.de

    This file is free source code; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
    COPYING for more details.
 */


#define BLACK          1
#define WHITE          2

#define COMPUTER       1
#define HUMAN          2

#define	NORMAL         1
#define	GAMMON         2
#define BACKGAMMON     4

#define COMPI_VS_COMPI 0
#define HUMAN_VS_COMPI 1
#define HUMAN_VS_HUMAN 2
#define LEARN          3
#define PLAY           4

/* for doubler() */
#define RESIGN END_OF_GAME
#define END_OF_GAME     10
#define	ACCEPT		11
#define ANSWER		 1
#define OFFER		 2
#define NODOUBLE	 0
#define   DOUBLE	 1
#define FIRSTMOVE	 2

/* special board areas */
#define BAR	   start_pin
#define OTHER_BAR  end_pin
#define FINISHED   (25+turn)
#define BLACK_BAR   0
#define WHITE_BAR  25
#define DOUBLER    30
#define DICE       31

/* "flags" */
#define NORMAL_GAME         0
#define ROLLOUT             2
#define EDITED_POSITION     3
#define LOADED_POSITION     4
#define COMPUTER_TOURNAMENT 5
#define REPLAY_GAME         6
#define	UPPER		    1
#define	LOWER		    2

/* where to put the endgame database */
#ifndef DATABASE
#define DATABASE "lib/xgammon.db"
#endif


extern int  break_loop, initialize, end_of_game, end_of_tournament;

extern int  turn, other, pash, to_move, roll[4];
extern int  direction, start_pin, end_pin;

typedef struct _Doubler {
	int value;
	int owner;
} Doubler;

extern Doubler doubler;

typedef struct _Board {
	Widget	widget;
	Window	window;
	GC	gc;
	Pixmap	pixmap;
	Pixmap	pin_pixmap[41];		/* 32 for pin 9 for bar */
	int	width, height, stone_width;
} BOARD;

extern BOARD	board;

typedef struct _X11Set {
	Display	     *dpy;
	GC	      gc, stoneGC[2], boardGC, diceGC;
	Cursor	      cursor;
	XFontStruct  *doubler_font, *small_font;
	XColor 	      fg_col, bg_col;
	Widget        toplevel; 
	Widget        button_shell, form;
	Widget        quit, file_button, option_button, game_button, menu, entry;
	Widget        text_display, tournament_display;
	BOARD         board;
} X11SET;

extern X11SET	X11Set;

typedef void (*MoveFunc)(int with_double);
	
struct _Player {
	char name[25];
	int beginner_of_game;
	int type;			/* human or compi */
	int points;
	int color;
	int gammons;
	int backgammons;
	MoveFunc MoveFunction;
	BOARD board;
	X11SET X11Set;
} Player[2];

struct  _PinTable {
        int count;
        int color;
} Pin[29], rollout_position[29];	/* 0, 25 = bar, 1 - 24 = board and 26, 27 finished[color] */


/* if you have the dice values 1 and 1 and all stones are on different
   pin and all completely movable there are almost 2000 possibilities to 
   set your stones, and sice a move contains 4 single steps ...

   this following list is sturctured in parts of four nodes
   not concerning if the dice are equal */

typedef struct _Move {
        int from;
        int to;
} MOVE;

MOVE possible_moves[8000], *list;

struct _move_hist {
	int	    from [4];
	int	    to   [4];
	int	    hit  [4];
	struct _move_hist *next;
};

extern struct _move_hist *move_hist;

struct _Tournament {
	unsigned int   game_number;
	unsigned int   winning_point;
} tournament;

struct _RolloutSave {
	int turn;
	int doubler_value;
	int doubler_owner;
	int roll[2];
} rollout_save;

FILE *endgame_database;

extern void switch_turn();

