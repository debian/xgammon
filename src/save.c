/*  save.c

    Copyright (C) 1994	Lambert Klasen & Detlef Steuer
			klasen@asterix.uni-muenster.de
			steuer@amadeus.statistik.uni-dortmund.de

    This file is free source code; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
    COPYING for more details.
 */



/*$Id: save.c,v 1.1 1999/10/05 21:37:37 delle Exp delle $*/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <X11/Intrinsic.h>

#include "xgammon.h"
#include "gammon.h"

extern void  print_dialog_text ();
extern void  Info              ();

extern int   done_yet;
extern float point_values[], prime_length_factor[];

void save (Widget w, XEvent *e, String *vector, Cardinal *count)
{
	FILE *save_file;
	char *filename = "xgammon.save";
	int  i;
	char kind = 0;
	
	if (done_yet) {
		Info ("Please don't move before saving");
		return;
	}
	
	/* The option 'p' (position) and 'g' (game) don't make that much a 
	   diffrence now, but there is planed a replay section, where
	   it shall be possible to step through the game and the two options
	   will be usefull again.
	   The saving is many the times easier to realize.
	   'r' means to save the result of a rollout.
	   'm' is saving invoked by menu-click.
	   In the latter cases the save file will not be default.
	*/
	  
	if      (*vector[0] == 'p') kind = 'p';
	else if (*vector[0] == 'g') kind = 'g';
	else if (*vector[0] == 'm') {
		kind = 'g';
		filename = vector[1];
	}
	else if (*vector[0] == 'r') {
		kind = 'p';
		filename = vector[1];
	}
	
	save_file = fopen (filename, "w");
	
	if (!save_file) {
		AppendDialogText (LOWER, "couldn't open save file!\nsorry!\n");
		return;
	}
	
	if (kind == 'g') {
		print_dialog_text(save_file);
	
		if      (gammon_resource.moneygame) fprintf (save_file, "money-game match between %s and %s:\n", Player[0].name, Player[1].name);
		else if (gammon_resource.winat)     fprintf (save_file, "%d point match between %s and %s:\n", gammon_resource.winat, Player[0].name, Player[1].name);
	
		fprintf (save_file, "%s has %d points, %s has %d points.\n", Player[0].name, Player[0].points, Player[1].name, Player[1].points);
		fprintf (save_file, "current game number: %d\n", tournament.game_number);
	}
	
	fprintf (save_file, "turn: %s\n", (turn == BLACK) ? "black" : "white");
	
	fprintf (save_file, "dice values: %d %d\n", abs(roll[0]), abs(roll[1]));
	fprintf (save_file, "doubler value: %d\n", doubler.value);
	fprintf (save_file, "last doubler: ");
	
	if      (doubler.owner == BLACK) fprintf (save_file, "black\n");
	else if (doubler.owner == WHITE) fprintf (save_file, "white\n");
	else 				fprintf (save_file, "none\n");
	
	fprintf (save_file, "on bar: black: %d, white: %d\n", Pin[0].count, Pin[25].count);
	
	for (i=1; i<25; i++) {
		if (Pin[i].color == WHITE) fprintf (save_file, "pin %d: %d white men\n", i, Pin[i].count);
		if (Pin[i].color == BLACK) fprintf (save_file, "pin %d: %d black men\n", i, Pin[i].count);
	}
	
	fclose (save_file);

	if (*vector[0] != 'r' && !gammon_resource.rollout)
		Info ("saving done\n");
}
	
void sig_save(int n)
{
	FILE *save_file;
	int i;
	
	save_file = fopen ("xgammon.sig_save", "w");
	
	if (!save_file) return;
	
	fprintf (save_file, "turn: ");
	
	if (turn == BLACK) fprintf (save_file, "black\n");
	else		   fprintf (save_file, "white\n");
	
	
	fprintf (save_file, "dice values: %d %d\n", roll[0], roll[1]);
	fprintf (save_file, "doubler value: %d\n", doubler.value);
	fprintf (save_file, "last doubler: ");
	
	if (doubler.owner) {
		if (doubler.owner == BLACK) fprintf (save_file, "black\n");
		else			   fprintf (save_file, "white\n");
	}
	else {
		fprintf (save_file, "none\n");
	}
	
	fprintf (save_file, "on bar: black: %d white: %d\n", Pin[0].count, Pin[25].count);
	
	for (i=1; i<25; i++) {
		if (Pin[i].color == WHITE) fprintf (save_file, "pin %d: %d white men\n", i, Pin[i].count);
		if (Pin[i].color == BLACK) fprintf (save_file, "pin %d: %d black men\n", i, Pin[i].count);
	}
	
	fclose(save_file);

#ifndef AIX
	fprintf (stderr, "xgammon: %s.  Sorry and bye!\n", sys_siglist[n]);
#else
	fprintf (stderr, "xgammon: Caught signal %d.   Sorry and bye!\n", n);
#endif

	/*abort ();*/
	exit  (n);
}

void save_config (void)
{
	FILE *f;
	int i;

	if (!(f = fopen ("xgammon.config", "w"))) {
		fprintf (stderr, "counldn't write config file\n");
		return;
	}

	fprintf (f, "point values:\n");

	for (i=1;  i<13; i++) fprintf (f, "%-2.3f, ", point_values[i]);
	fprintf (f, "\n");
	for (i=13; i<25; i++) fprintf (f, "%-2.3f, ", point_values[i]);
	fprintf (f, "\n\n");
	
	fprintf (f, "prime length factors:\n");

	for (i=0;  i<8; i++) fprintf (f, "%-2.3f, ", prime_length_factor[i]);
	fprintf (f, "\n");
}
