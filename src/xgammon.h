/*  xgammon.h

    Copyright (C) 1994	Lambert Klasen & Detlef Steuer
			klasen@asterix.uni-muenster.de
			steuer@amadeus.statistik.uni-dortmund.de

    This file is free source code; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
    COPYING for more details.
 */

#define WaitForEvent(event_type) while(1) {\
					XEvent event;\
					XtAppNextEvent(app_con, &event);\
					if(event.type == event_type) {\
						XtDispatchEvent(&event);\
						break;\
					}\
					XtDispatchEvent(&event);\
				}

#define dice_width stone_width

extern char		add_text[];
extern XtAppContext     app_con;

extern int		width, height;
extern int		stone_width, stone_height;

struct _gammon_resource {
	char	*other_display;
	char    *board_geometry;
        Pixel    board_Pixel;
        char    *board_color;
        Pixel    light_Pixel;
        char    *light_color;
        Pixel    dark_Pixel;
        char    *dark_color;
        Pixel    bar_Pixel;
        char    *bar_color;
        Pixel    white_Pixel;
        char    *white_color;
        Pixel    black_Pixel;
        char    *black_color;
        char    *small_font;
        char    *doubler_font;
        char    *human_stone;
        char    *gamekind;
	int	protokol;        
        int     stone_steps;
        int     delaytime;
        int     watchmove;
	int	autoplay;        
        int     doubling;
	int	getdice;
        int     moneygame;
        int     bestof;
        int     winat;
        int     number_of_games;
        int     mutations;
        int     rollout;
        int     num_rollouts;
	char	*position_file;
	char	*database;
	char	*server;
	int     port;
	int     button_move;
} gammon_resource;

/* diawin.c */
extern void AppendDialogText ();

/* drawing.c */
extern void PutStone         ();
extern void RemoveStone      ();
