/*  edit.c

    Copyright (C) 1994	Lambert Klasen & Detlef Steuer
			klasen@asterix.uni-muenster.de
			steuer@amadeus.statistik.uni-dortmund.de

    This file is free source code; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
    COPYING for more details.
 */



#include <stdio.h>
#include <stdlib.h>
#include <X11/Intrinsic.h>
#include <X11/Composite.h>
#include <X11/Shell.h>
#include <X11/StringDefs.h>

#include <X11/Xaw/Form.h>
#include <X11/Xaw/Command.h>
#include <X11/Xaw/Label.h>

#include "xgammon.h"
#include "gammon.h"

static void GetGameValuesPopup ();

extern void DrawEmptyBoard     ();
extern void DrawBoard          ();
extern int  EventToPin         ();

void EditPosition (void)
{
	XEvent event;
	int black_count = 0, white_count = 0, p;

	for (p = 0; p<28; p++) {
		Pin[p].color = 0;
		Pin[p].count = 0;
	}

	DrawEmptyBoard ();
	DrawBoard ();

	AppendDialogText (LOWER, "\n\nedit a position:\npress left button to place a black stone,\nright button tp place a white stone,\nthe middle button to remove a stone,\nany key to break.\n");

	while (1) {
	XtAppNextEvent (app_con, &event);
	if (event.xbutton.window == Player[0].X11Set.board.window) {
		if (event.type == ButtonRelease) {
			p = EventToPin (event.xbutton.x, event.xbutton.y);
			if (event.xbutton.button == 1) {
				if (Pin[p].color != WHITE && black_count<15) {
					black_count ++;
					PutStone (BLACK, p);
				}
			}
			else if (event.xbutton.button == 3) {
				if (p == BAR && white_count<15) {
					white_count ++;
					PutStone (WHITE, OTHER_BAR);
				}
				else if (Pin[p].color != BLACK && white_count<15) {
					white_count ++;
					PutStone (WHITE, p);
				}
			}
			else if (event.xbutton.button == 2) {
				if (Pin[p].color) {
					if      (Pin[p].color == BLACK) black_count --;
					else if (Pin[p].color == WHITE) white_count --;
					RemoveStone (p);
				}
			}
			if (black_count + white_count == 30) break;
		}
		else if (event.type == KeyPress) break;
	}
	else XtDispatchEvent (&event);
	}

	GetGameValuesPopup ();

	if (black_count<15) {
		int i, t = turn;	/* PutStone  is too dump */
		turn = BLACK;
		for (i=black_count; i<15; i++) PutStone (BLACK, FINISHED);
		turn = t;
	}
	if (white_count<15) {
		int i, t = turn;
		turn = WHITE;
		for (i=white_count; i<15; i++) PutStone (WHITE, FINISHED);
		turn = t;
	}

	initialize = EDITED_POSITION;	/* set some global flags */
	break_loop = EDITED_POSITION;
	end_of_game = 1;
}

static void GetGameValuesPopup (void)
{
	Widget toplevel = Player[0].X11Set.toplevel;
	Widget popup_shell, form;
	Widget turn_label,  doubler_label,  owner_label,  roll_label;
	Widget turn_switch, doubler_switch, owner_switch, roll_switch[2];
	Widget ok;

	Arg       args[4];
	char      value[15];
	Dimension w, h;
	Position  x, y;
	int       ignore_dice = 0;

	XtSetArg (args[0], XtNx, &x);
	XtSetArg (args[1], XtNy, &y);
	XtGetValues (toplevel, args, 2);
	XtSetArg (args[0], XtNx, x+50);
	XtSetArg (args[1], XtNy, y+50);
	popup_shell = XtCreatePopupShell ("Edit Position", transientShellWidgetClass, toplevel, args, 2);
	form = XtCreateManagedWidget ("form", formWidgetClass, popup_shell, NULL, 0);

	turn_label = XtCreateManagedWidget ("turn:         ", labelWidgetClass, form, NULL, 0);

	XtSetArg (args[0], XtNfromVert, turn_label);
	doubler_label  = XtCreateManagedWidget ("doubler value:", labelWidgetClass, form, args, 1);

	XtSetArg (args[0], XtNfromVert, doubler_label);
	owner_label = XtCreateManagedWidget ("last doubler: ", labelWidgetClass, form, args, 1);

	XtSetArg (args[0], XtNfromVert, owner_label);
	roll_label = XtCreateManagedWidget ("dice:         ", commandWidgetClass, form, args, 1);

	XtSetArg (args[0], XtNfromVert, roll_label);
	ok	   = XtCreateManagedWidget ("ok",    commandWidgetClass, form, args, 1);

	XtSetArg (args[0], XtNfromHoriz, turn_label);
	if (turn == BLACK) strcpy (value,"black");
	else  strcpy (value,"white");
	turn_switch    = XtCreateManagedWidget (value, commandWidgetClass, form, args, 1);

	XtSetArg (args[0], XtNwidth, &w);
	XtSetArg (args[1], XtNheight, &h);
	XtGetValues (turn_switch, args, 2);
	XtSetArg (args[2], XtNwidth, w);
	XtSetArg (args[3], XtNheight, h);

	XtSetArg (args[0], XtNfromHoriz, doubler_label);
	XtSetArg (args[1], XtNfromVert, turn_label);
	sprintf (value, "%d", doubler.value);
	doubler_switch = XtCreateManagedWidget (value, commandWidgetClass, form, args, 4);

	XtSetArg (args[0], XtNfromHoriz, owner_label);
	XtSetArg (args[1], XtNfromVert, doubler_label);
	if (!doubler.owner) strcpy (value,"none");
	else if (doubler.owner == BLACK)  strcpy (value,"black");
	else  strcpy (value,"white");
	owner_switch   = XtCreateManagedWidget (value, commandWidgetClass, form, args, 4);

{	unsigned short d;
	XtSetArg (args[0], XtNhorizDistance, &d);
	XtGetValues (form, args, 1);

	XtSetArg (args[0], XtNfromHoriz, roll_label);
	XtSetArg (args[1], XtNfromVert, owner_label);
	XtSetArg (args[2], XtNwidth, w/2 - d/2);
}

	roll[0] =  abs (roll[0]);
	roll[1] =  abs (roll[1]);
	sprintf (value, "%d", roll[0]);
	roll_switch[0] = XtCreateManagedWidget (value, commandWidgetClass, form, args, 4);

	XtSetArg (args[0], XtNfromHoriz, roll_switch[0]);
	XtSetArg (args[1], XtNfromVert, owner_label);
	sprintf (value, "%d", roll[1]);
	roll_switch[1] = XtCreateManagedWidget (value, commandWidgetClass, form, args, 4);

	XtPopup (popup_shell, XtGrabExclusive);

	while (1) {
		XEvent event;
		XtAppNextEvent (app_con, &event);
		if (event.type == ButtonRelease) {
			if (event.xbutton.window == XtWindow (turn_switch)) {
				if (turn == BLACK) {
					turn = WHITE;
					XtSetArg (args[0], XtNlabel, "white");
				}
				else    {
					turn = BLACK;
					XtSetArg (args[0], XtNlabel, "black");
				}
				XtSetValues (turn_switch, args, 1);
			}
			else if (event.xbutton.window == XtWindow (doubler_switch)) {
				doubler.value *= 2;
				if (doubler.value > 64) doubler.value = 1;
				sprintf (value, "%d", doubler.value);
				XtSetArg (args[0], XtNlabel, value);
				XtSetValues (doubler_switch, args, 1);
			}
			else if (event.xbutton.window == XtWindow (owner_switch)) {
				if (!doubler.owner) {
					strcpy (value, "black");
					doubler.owner = BLACK;
				}
				else if (doubler.owner == BLACK) {
					strcpy (value, "white");
					doubler.owner = WHITE;
				}
				else  {
					strcpy (value, "none");
					doubler.owner = 0;
				}
				XtSetArg (args[0], XtNlabel, value);
				XtSetValues (owner_switch, args, 1);
			}
			else if (event.xbutton.window == XtWindow (roll_label)) {
				if (!ignore_dice) {
					ignore_dice = 1;
					strcpy (value, "ignore roll   ");
				}
				else {
					ignore_dice = 0;
					strcpy (value, "dice:         ");
				}
				XtSetArg (args[0], XtNlabel, value);
				XtSetValues (roll_label, args, 1);
			}
			else if (event.xbutton.window == XtWindow (roll_switch[0])) {
				roll[0] ++;
				if (roll[0] == 7) roll[0] = 1;
				sprintf (value, "%d", roll[0]);
				XtSetArg (args[0], XtNlabel, value);
				XtSetValues (roll_switch[0], args, 1);
			}
			else if (event.xbutton.window == XtWindow (roll_switch[1])) {
				roll[1] ++;
				if (roll[1] == 7) roll[1] = 1;
				sprintf (value, "%d", roll[1]);
				XtSetArg (args[0], XtNlabel, value);
				XtSetValues (roll_switch[1], args, 1);
			}
			else if (event.xbutton.window == XtWindow (ok))
				break;
		}
		XtDispatchEvent (&event);
	}

	XtPopdown (popup_shell);

	switch_turn (); switch_turn ();		/* set rest of vars */

	if (turn == WHITE) { 
		roll[0]	*= -1; 
		roll[1] *= -1; 
	}

	if (ignore_dice) {
		roll[0] = (rand ()%6 + 1) * direction;
		roll[1] = (rand ()%6 + 1) * direction;
	}
	if (roll[0] == roll[1]) {
		roll[3] = roll[2] = roll[0]; 
		pash    = 1; 
		to_move = 4; 
	} else {
		roll[3] = roll[2] = 0; 
		pash    = 0; 
		to_move = 2; 
	}
}
